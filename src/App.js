import "./App.css";
import { Routes, Route, Outlet } from "react-router-dom";
import Layout from "./layout/Layout";
import SignIn from "./signin/SignIn";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<SignIn />} />
        <Route path="/*" element={<Layout />} />
      </Routes>
    </>
  );
}

export default App;
