import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  forms: [],
  form: {},
};

export const formSlice = createSlice({
  name: "form",
  initialState,
  reducers: {
    add: (state, action) => {
      state.forms.push(action.payload);
    },
    find: (state, action) => {
      state.form = state.forms.find((value) => value.id === action.payload);
    },
    remove: (state, action) => {
      state.forms = state.forms.filter((value) => value.id != action.payload);
    },

    update: (state, action) => {
      state.forms.map((form) => {
        console.log(action.payload, form);
        if (form.id === action.payload.id) {
          form.name = action.payload.name;
          form.description = action.payload.description;
          form.price = action.payload.price;
          form.image = action.payload.image;
          console.log(form);
          return form;
        }
        return form;
      });
    },
  },
});

// Action creators are generated for each case reducer function
export const { add, find, remove, update } = formSlice.actions;

export default formSlice.reducer;
