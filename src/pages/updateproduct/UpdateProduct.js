import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { update } from "../../redux/reducers/formSlice";

const UpdateProduct = () => {
  const { id } = useParams();
  const forms = useSelector((state) => state.form.forms);
  const productExisting = forms.filter((p) => p.id == id);
  const { name, price, image, description } = productExisting[0];
  const [product, setProduct] = useState({
    name: name,
    description: description,
    price: price,
    image: image,
  });
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleUpdate = (e) => {
    e.preventDefault();
    const data = {
      id: id,
      name: product.name,
      price: product.price,
      image: product.image,
      description: product.description,
    };
    console.log(data);
    dispatch(update(data));
    //navigate("/Home");
  };
  return (
    <>
      <form className="container mt-5">
        <div className="row">
          <div className="col-md-6">
            <div className="mb-3">
              <label htmlFor="nom" className="form-label">
                Nom du Plat
              </label>
              <input
                type="text"
                className="form-control"
                id="nom"
                name="name"
                value={product.name}
                onChange={(e) =>
                  setProduct({ ...product, name: e.target.value })
                }
              />
            </div>
          </div>
          <div className="col-md-6">
            <div className="mb-3">
              <label htmlFor="infos" className="form-label">
                libellé
              </label>
              <input
                type="text"
                className="form-control"
                id="infos"
                name="description"
                value={product.description}
                onChange={(e) =>
                  setProduct({ ...product, description: e.target.value })
                }
              />
            </div>
          </div>
        </div>

        <div className="mb-3">
          <label htmlFor="prix" className="form-label">
            Prix
          </label>
          <input
            type="number"
            className="form-control"
            id="prix"
            name="price"
            value={product.price}
            onChange={(e) => setProduct({ ...product, price: e.target.value })}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="image" className="form-label">
            Image
          </label>
          <input
            type="file"
            className="form-control"
            id="image"
            name="image"
            onChange={(e) => setProduct({ ...product, image: e.target.value })}
          />
        </div>

        <div className="mb-3">
          <button type="submit" className="btn " onClick={handleUpdate}>
            Submit
          </button>
        </div>
      </form>
    </>
  );
};

export default UpdateProduct;
