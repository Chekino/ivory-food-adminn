import React from "react";
import { Routes, Route, Outlet } from "react-router-dom";
import AdminNav from "../header/AdminNav";
import AddProduct from "../pages/addproduct/AddProduct";
import UpdateProduct from "../pages/updateproduct/UpdateProduct";
import DeleteProduct from "../pages/deleteproduct/DeleteProduct";
import Home from "../pages/home/Home";

const Layout = () => {
  return (
    <div>
      <AdminNav />
      <Routes>
        <Route path="/Home" element={<Home />} />
        <Route path="/addproduct" element={<AddProduct />} />
        <Route path="/updateproduct/:id" element={<UpdateProduct />} />
      </Routes>
    </div>
  );
};

export default Layout;
