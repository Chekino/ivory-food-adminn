import React from "react";
import { Navbar, Nav, Container } from "react-bootstrap";
import { RiAddLine, RiDeleteBinLine, RiEditLine } from "react-icons/ri";
import "./adminnav.css"; // Importe le fichier CSS personnalisé
import { Link, Outlet } from "react-router-dom";

function AdminNav() {
  return (
    <Navbar className="navbar-orange" expand="lg">
      <Container>
        <Navbar.Brand href="#">
          <img
            src="/path/to/your/logo.png"
            width="30"
            height="30"
            className="d-inline-block align-top"
            alt="Logo"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mx-auto">
            <Nav.Link href="#">
              <Link to="/Home">
                <RiAddLine className="nav-icon " /> Dashboard
              </Link>
            </Nav.Link>
            <Nav.Link href="#">
              <Link to="/addproduct">
                <RiAddLine className="nav-icon " /> Ajouter un produit
              </Link>
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default AdminNav;
